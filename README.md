<img src="https://awoo.systems/trans_rights_badge.svg"/><br>
## clj-simplex

A Clojure implementation of Simplex noise.

# Usage

```
(require 'simplex.noise)
(simplex.noise/seed)
(simplex.noise/noise 0.1 0.2)
```

`(seed)` accepts an argument to initialize the pseudo-random generator.
`(noise)` can be called with 1, 2, 3 or 4 dimensions.
