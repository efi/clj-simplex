(ns simplex.noise)

(defrecord Grad3 [^double x ^double y ^double z])
(defrecord Grad4 [^double x ^double y ^double z ^double w])

(def grad3
  (list
    (Grad3.  1  1  0)
    (Grad3. -1  1  0)
    (Grad3.  1 -1  0)
    (Grad3. -1 -1  0)
    (Grad3.  1  0  1)
    (Grad3. -1  0  1)
    (Grad3.  1  0 -1)
    (Grad3. -1  0 -1)
    (Grad3.  0  1  1)
    (Grad3.  0 -1  1)
    (Grad3.  0  1 -1)
    (Grad3.  0 -1 -1)))

(def grad4
  (list
    (Grad4.  0  1  1  1)
    (Grad4.  0  1  1 -1)
    (Grad4.  0  1 -1  1)
    (Grad4.  0  1 -1 -1)
    (Grad4.  0 -1  1  1)
    (Grad4.  0 -1  1 -1)
    (Grad4.  0 -1 -1  1)
    (Grad4.  0 -1 -1 -1)
    (Grad4.  1  0  1  1)
    (Grad4.  1  0  1 -1)
    (Grad4.  1  0 -1  1)
    (Grad4.  1  0 -1 -1)
    (Grad4. -1  0  1  1)
    (Grad4. -1  0  1 -1)
    (Grad4. -1  0 -1  1)
    (Grad4. -1  0 -1 -1)
    (Grad4.  1  1  0  1)
    (Grad4.  1  1  0 -1)
    (Grad4.  1 -1  0  1)
    (Grad4.  1 -1  0 -1)
    (Grad4. -1  1  0  1)
    (Grad4. -1  1  0 -1)
    (Grad4. -1 -1  0  1)
    (Grad4. -1 -1  0 -1)
    (Grad4.  1  1  1  0)
    (Grad4.  1  1 -1  0)
    (Grad4.  1 -1  1  0)
    (Grad4.  1 -1 -1  0)
    (Grad4. -1  1  1  0)
    (Grad4. -1  1 -1  0)
    (Grad4. -1 -1  1  0)
    (Grad4. -1 -1 -1  0)))

(def ip
  (apply concat
    (repeat 2
      (list
        151 160 137 91  90  15  131 13  201 95
        96  53  194 233 7   225 140 36  103 30  69  142 8   99  37
        240 21  10  23  190 6   148 247 120 234 75  0   26  197 62
        94  252 219 203 117 35  11  32  57  177 33  88  237 149 56
        87  174 20  125 136 171 168 68  175 74  165 71  134 139
        48  27  166 77  146 158 231 83  111 229 122 60  211 133
        230 220 105 92  41  55  46  245 40  244 102 143 54  65  25
        63  161 1   216 80  73  209 76  132 187 208 89  18  169 200
        196 135 130 116 188 159 86  164 100 109 198 173 186 3
        64  52  217 226 250 124 123 5   202 38  147 118 126 255
        82  85  212 207 206 59  227 47  16  58  17  182 189 28  42
        223 183 170 213 119 248 152 2   44  154 163 70  221 153
        101 155 167 43  172 9   129 22  39  253 19  98  108 110 79
        113 224 232 178 185 112 104 218 246 97  228 251 34  242
        193 238 210 144 12  191 179 162 241 81  51  145 235 249
        14  239 107 49  192 214 31  181 199 106 157 184 84  204
        176 115 121 50  45  127 4   150 254 138 236 205 93  222
        114 67  29  24  72  243 141 128 195 78  66  215 61  156 180))))

(def F2 (* 0.5 (- (Math/sqrt 3.0) 1.0)))
(def G2 (/ (- 3.0 (Math/sqrt 3.0)) 6.0))
(def F3 (/ 1.0 3.0))
(def G3 (/ 1.0 6.0))
(def F4 (/ (- (Math/sqrt 5.0) 1.0) 4.0))
(def G4 (/ (- 5.0 (Math/sqrt 5.0)) 20.0))

(defn- fastfloor [x]
  (let [xi (int x)]
    (if (< x xi)
      (- xi 1)
      xi)))

(defn- dot
  ([g x y]      (+ (* (.-x g) x) (* (.-y g) y)))
  ([g x y z]    (+ (* (.-x g) x) (* (.-y g) y) (* (.-z g) z)))
  ([g x y z w]  (+ (* (.-x g) x) (* (.-y g) y) (* (.-z g) z) (* (.-w g) w))))

(defn- ++ [m k] (update m k inc))

(declare p)

(defn- snoise
  ([x y]
    (let [s (* F2 (+ x y))
          i (fastfloor (+ x s))
          j (fastfloor (+ y s))
          t (* G2 (+ i j))
          x0 (- x (- i t))
          y0 (- y (- j t))
          [i1 j1] (if (> x0 y0) [1 0] [0 1])
          x1 (+ (- x0  i1)        G2)
          y1 (+ (- y0  j1)        G2)
          x2 (+ (- x0 1.0) (* 2.0 G2))
          y2 (+ (- y0 1.0) (* 2.0 G2))
          ii (bit-and i 255)
          jj (bit-and j 255)
          gi0 (mod (.get p (+ ii    (.get p jj)))        12)
          gi1 (mod (.get p (+ ii i1 (.get p (+ jj j1)))) 12)
          gi2 (mod (.get p (+ ii  1 (.get p (+ jj  1)))) 12)
          t0 (- 0.5 (* x0 x0) (* y0 y0))
          n0 (if (< t0 0)
               0.0
               (* (Math/pow t0 4) (dot (nth grad3 gi0) x0 y0)))
          t1 (- 0.5 (* x1 x1) (* y1 y1))
          n1 (if (< t1 0)
               0.0
               (* (Math/pow t1 4) (dot (nth grad3 gi1) x1 y1)))
          t2 (- 0.5 (* x2 x2) (* y2 y2))
          n2 (if (< t2 0.0)
               0.0
               (* (Math/pow t2 4) (dot (nth grad3 gi2) x2 y2)))]
      (* 70.0 (+ n0 n1 n2))))
  ([x y z]
    (let [s (* F3 (+ x y z))
          i (fastfloor (+ x s))
          j (fastfloor (+ y s))
          k (fastfloor (+ z s))
          t (* G3 (+ i j k))
          x0 (- x (- i t))
          y0 (- y (- j t))
          z0 (- z (- k t))
          [i1 j1 k1 i2 j2 k2]
            (if (>= x0 y0)
              (if (>= y0 z0)
                [1 0 0 1 1 0]
                (if (>= x0 z0)
                  [1 0 0 1 0 1]
                  [0 0 1 1 0 1]))
              (if (< y0 z0)
                [0 0 1 0 1 1]
                (if (< x0 z0)
                  [0 1 0 0 1 1]
                  [0 1 0 1 1 0])))
          x1 (+ (- x0  i1)        G3)
          y1 (+ (- y0  j1)        G3)
          z1 (+ (- z0  k1)        G3)
          x2 (+ (- x0  i2) (* 2.0 G3))
          y2 (+ (- y0  j2) (* 2.0 G3))
          z2 (+ (- z0  k2) (* 2.0 G3))
          x3 (+ (- x0 1.0) (* 3.0 G3))
          z3 (+ (- z0 1.0) (* 3.0 G3))
          y3 (+ (- y0 1.0) (* 3.0 G3))
          ii (bit-and i 255)
          jj (bit-and j 255)
          kk (bit-and k 255)
          gi0 (mod (.get p (+ ii    (.get p (+ jj    (.get p kk)))))        12)
          gi1 (mod (.get p (+ ii i1 (.get p (+ jj j1 (.get p (+ kk k1)))))) 12)
          gi2 (mod (.get p (+ ii i2 (.get p (+ jj j2 (.get p (+ kk k2)))))) 12)
          gi3 (mod (.get p (+ ii  1 (.get p (+ jj  1 (.get p (+ kk  1)))))) 12)
          t0 (- 0.6 (* x0 x0) (* y0 y0) (* z0 z0))
          n0 (if (< t0 0)
               0.0
               (* (Math/pow t0 4) (dot (nth grad3 gi0) x0 y0 z0)))
          t1 (- 0.6 (* x1 x1) (* y1 y1) (* z1 z1))
          n1 (if (< t1 0)
               0.0
               (* (Math/pow t1 4) (dot (nth grad3 gi1) x1 y1 z1)))
          t2 (- 0.6 (* x2 x2) (* y2 y2) (* z2 z2))
          n2 (if (< t2 0)
               0.0
               (* (Math/pow t2 4) (dot (nth grad3 gi2) x2 y2 z2)))
          t3 (- 0.6 (* x3 x3) (* y3 y3) (* z3 z3))
          n3 (if (< t3 0)
               0.0
               (* (Math/pow t3 4) (dot (nth grad3 gi3) x3 y3 z3)))]
      (* 32.0 (+ n0 n1 n2 n3))))
  ([x y z w]
    (let [s (* F4 (+ x y z w))
          i (fastfloor (+ x s))
          j (fastfloor (+ y s))
          k (fastfloor (+ z s))
          l (fastfloor (+ w s))
          t (* G4 (+ i j k l))
          x0 (- x (- i t))
          y0 (- y (- j t))
          z0 (- z (- k t))
          w0 (- w (- l t))
          [i1 j1 k1 l1 i2 j2 k2 l2 i3 j3 k3 l3]
            (as-> {:rx 0 :ry 0 :rz 0 :rw 0} score
              (if (> x0 y0)
                (++ score :rx)
                (++ score :ry))
              (if (> x0 z0)
                (++ score :rx)
                (++ score :rz))
              (if (> x0 w0)
                (++ score :rx)
                (++ score :rw))
              (if (> y0 z0)
                (++ score :ry)
                (++ score :rz))
              (if (> y0 w0)
                (++ score :ry)
                (++ score :rw))
              (if (> z0 w0)
                (++ score :rz)
                (++ score :rw))
              [(if (>= (:rx score) 3) 1 0)
               (if (>= (:ry score) 3) 1 0)
               (if (>= (:rz score) 3) 1 0)
               (if (>= (:rw score) 3) 1 0)
               (if (>= (:rx score) 2) 1 0)
               (if (>= (:ry score) 2) 1 0)
               (if (>= (:rz score) 2) 1 0)
               (if (>= (:rw score) 2) 1 0)
               (if (>= (:rx score) 1) 1 0)
               (if (>= (:ry score) 1) 1 0)
               (if (>= (:rz score) 1) 1 0)
               (if (>= (:rw score) 1) 1 0)])
          x1 (+ (- x0  i1)        G4)
          y1 (+ (- y0  j1)        G4)
          z1 (+ (- z0  k1)        G4)
          w1 (+ (- w0  l1)        G4)
          x2 (+ (- x0  i2) (* 2.0 G4))
          y2 (+ (- y0  j2) (* 2.0 G4))
          z2 (+ (- z0  k2) (* 2.0 G4))
          w2 (+ (- w0  l2) (* 2.0 G4))
          x3 (+ (- x0  i3) (* 3.0 G4))
          y3 (+ (- y0  j3) (* 3.0 G4))
          z3 (+ (- z0  k3) (* 3.0 G4))
          w3 (+ (- w0  l3) (* 3.0 G4))
          x4 (+ (- x0 1.0) (* 4.0 G4))
          y4 (+ (- y0 1.0) (* 4.0 G4))
          z4 (+ (- z0 1.0) (* 4.0 G4))
          w4 (+ (- w0 1.0) (* 4.0 G4))
          ii (bit-and i 255)
          jj (bit-and j 255)
          kk (bit-and k 255)
          ll (bit-and l 255)
          gi0 (mod (.get p (+ ii    (.get p (+ jj    (.get p (+ kk    (.get p ll)))))))        32)
          gi1 (mod (.get p (+ ii i1 (.get p (+ jj j1 (.get p (+ kk k1 (.get p (+ ll l1)))))))) 32)
          gi2 (mod (.get p (+ ii i2 (.get p (+ jj j2 (.get p (+ kk k2 (.get p (+ ll l2)))))))) 32)
          gi3 (mod (.get p (+ ii i3 (.get p (+ jj j3 (.get p (+ kk k3 (.get p (+ ll l3)))))))) 32)
          gi4 (mod (.get p (+ ii  1 (.get p (+ jj  1 (.get p (+ kk  1 (.get p (+ ll  1)))))))) 32)
          t0 (- 0.6 (* x0 x0) (* y0 y0) (* z0 z0) (* w0 w0))
          n0 (if (< t0 0)
               0.0
               (* (Math/pow t0 4) (dot (nth grad4 gi0) x0 y0 z0 w0)))
          t1 (- 0.6 (* x1 x1) (* y1 y1) (* z1 z1) (* w1 w1))
          n1 (if (< t1 0)
               0.0
               (* (Math/pow t1 4) (dot (nth grad4 gi1) x1 y1 z1 w1)))
          t2 (- 0.6 (* x2 x2) (* y2 y2) (* z2 z2) (* w2 w2))
          n2 (if (< t2 0)
               0.0
               (* (Math/pow t2 4) (dot (nth grad4 gi2) x2 y2 z2 w2)))
          t3 (- 0.6 (* x3 x3) (* y3 y3) (* z3 z3) (* w3 w3))
          n3 (if (< t3 0)
               0.0
               (* (Math/pow t3 4) (dot (nth grad4 gi3) x3 y3 z3 w3)))
          t4 (- 0.6 (* x4 x4) (* y4 y4) (* z4 z4) (* w4 w4))
          n4 (if (< t4 0)
               0.0
               (* (Math/pow t4 4) (dot (nth grad4 gi4) x4 y4 z4 w4)))]
      (* 27.0 (+ n0 n1 n2 n3 n4)))))

(defn noise
  ([x] (noise x 0))
  ([x y]
    (+ 0.5 (* 0.5 (snoise x y))))
  ([x y z]
    (+ 0.5 (* 0.5 (snoise x y z))))
  ([x y z w]
    (+ 0.5 (* 0.5 (snoise x y z w)))))

(defn seed
  ([] (seed (java.lang.System/currentTimeMillis)))
  ([s]
    (let [r (java.util.Random. s)
          sp (java.util.ArrayList. ip)]
      (java.util.Collections/shuffle sp r)
      (def p
        (map #(bit-and % 255)
             sp)))))
 
#_(defn -main []
  (seed)
  (with-open [f (clojure.java.io/writer "output.pgm")]
    (.write f "P2\n256 256\n65535\n")
    (doseq [x (range 256)
            y (range 256)]
      (.write f (str (int (* 65535 (noise (/ x 8) (/ y 8)))) " "))
      (when (>= x 256) (.write f "\n")))))
