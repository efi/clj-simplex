(defproject org.clojars.efi/simplex "0.2"
  :description "A Clojure implementation of Simplex noise"
  :url "https://git.lubar.me/efi/clj-simplex"
  :license {:name "EPL-2.0 OR GPL-2.0-or-later WITH Classpath-exception-2.0"
            :url "https://www.eclipse.org/legal/epl-2.0/"}
  :dependencies [[org.clojure/clojure "1.10.0"]]
  :repl-options {:init-ns simplex.noise})
